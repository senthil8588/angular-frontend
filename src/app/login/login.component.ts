import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "../service/api.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  invalidLogin: boolean = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) { }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    const loginPayload = {
      email: this.loginForm.controls.email.value,
      password: this.loginForm.controls.password.value
    }
    this.apiService.login(loginPayload).subscribe(data => {
      console.log(data);
        if(data.status == 200) {
        console.log(data.status);
        window.localStorage.setItem('remember_token', data.remember_token);
        this.router.navigate(['list-user-ucars']);
      }else {
        this.invalidLogin = true;
        //alert(data.message);
      }
    });
  }

  ngOnInit() {
    if(window.localStorage.getItem('remember_token')) {
      this.router.navigate(['list-user-ucars']);
      return;
    }
    window.localStorage.removeItem('remember_token');
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required]
    });
  }

  register(): void {
    this.router.navigate(['register']);
  };

}
