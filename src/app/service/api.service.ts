import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Ucar} from "../model/ucar.model";
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api.response";

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) {  }
  baseUrl: string = 'http://localhost:4200/';

  login(loginPayload: { email: any; password: any; }) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>('http://localhost:4200/' + 'api/login', loginPayload);
  }
  register(registerPayload: { person_name: any; email: any; password: any; cpassword: any; }) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>('http://localhost:4200/' + 'api/register', registerPayload);
  }
  getAllUcars() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl+'api/ucarslist');
  }
    getUcars() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl+'api/ucars');
  }

  getUcarsById(id: number): Observable<ApiResponse> { 
    return this.http.get<ApiResponse>(this.baseUrl +'api/ucars/'+ id);
  }
	
  createUcars(ucars: Ucar): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl+'api/ucars', ucars);  }

  updateUcars(ucars: Ucar): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl+'api/ucars/' + ucars.id, ucars);
  }

  deleteUcars(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl+'api/ucars/' + id);
  }
}
