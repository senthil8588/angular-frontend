import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {AddUcarsComponent} from "./ucars/add-ucars/add-ucars.component";
import {ListUcarsComponent} from "./ucars/list-ucars/list-ucars.component";
import {EditUcarsComponent} from "./ucars/edit-ucars/edit-ucars.component";
import {ListUserUcarsComponent} from "./ucars/list-user-ucars/list-user-ucars.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'add-ucars', component: AddUcarsComponent },
  { path: 'list-ucars', component: ListUcarsComponent },
  { path: 'edit-ucars', component: EditUcarsComponent },
  { path: 'list-user-ucars', component: ListUserUcarsComponent },
  {path : '', component : ListUcarsComponent},


];

export const routing = RouterModule.forRoot(routes);
