import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "../service/api.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  invalidregister: boolean = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) { }

  onSubmit() {
    if (this.registerForm.invalid) {
      return;
    }

    const registerPayload = {
      person_name: this.registerForm.controls.person_name.value,
      email: this.registerForm.controls.email.value,
      password: this.registerForm.controls.password.value,
      cpassword: this.registerForm.controls.cpassword.value
    }
    this.apiService.register(registerPayload).subscribe(data => {
      console.log(data);
        if(data.status == 200) {
        console.log(data.status);
        window.localStorage.setItem('remember_token', data.remember_token);
        this.router.navigate(['list-user-ucars']);
      }else {
        this.invalidregister = true;
        alert(data.message);
      }
    });
  }

  ngOnInit() {

    window.localStorage.removeItem('remember_token');
    
    this.registerForm = this.formBuilder.group({
      person_name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      cpassword: ['', Validators.required]
    });
  }



}
