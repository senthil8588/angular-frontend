import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { ListUcarsComponent } from './ucars/list-ucars/list-ucars.component';
import { ListUserUcarsComponent } from './ucars/list-user-ucars/list-user-ucars.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AddUcarsComponent } from './ucars/add-ucars/add-ucars.component';
import { EditUcarsComponent } from './ucars/edit-ucars/edit-ucars.component';
import {routing} from "./app.routing";
import {FormsModule,ReactiveFormsModule} from "@angular/forms";
import {ApiService} from "./service/api.service";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {TokenInterceptor} from "./core/interceptor";

@NgModule({
  declarations: [
    BrowserModule,
    CommonModule,
    AppComponent,
    ListUcarsComponent,
    ListUserUcarsComponent,
    LoginComponent,
    RegisterComponent,
    AddUcarsComponent,
    EditUcarsComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ApiService, {provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi : true}],
  bootstrap: [AppComponent]
})
export class AppModule { }