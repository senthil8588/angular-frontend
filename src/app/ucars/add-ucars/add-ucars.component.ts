import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "../../service/api.service";

@Component({
  selector: 'app-add-ucars',
  templateUrl: './add-ucars.component.html',
  styleUrls: ['./add-ucars.component.css']
})
export class AddUcarsComponent implements OnInit {

  addForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [],
      carname: ['', Validators.required],
      carmodel: ['', Validators.required],
      company_name: ['', Validators.required],
      color: ['', Validators.required],
      mileage: ['', Validators.required],
      seating_capacity: ['', Validators.required],
      steering_type: ['', Validators.required],
      displacement_cc: ['', Validators.required],
      year: ['', Validators.required],
    });

  }

  onSubmit() {
    this.apiService.createUcars(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['list-user-ucars']);
      });
  }

}
