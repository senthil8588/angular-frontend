import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUcarsComponent } from './add-ucars.component';

describe('AddUcarsComponent', () => {
  let component: AddUcarsComponent;
  let fixture: ComponentFixture<AddUcarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUcarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUcarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
