import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUserUcarsComponent } from './list-user-ucars.component';

describe('ListUserUcarsComponent', () => {
  let component: ListUserUcarsComponent;
  let fixture: ComponentFixture<ListUserUcarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUserUcarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUserUcarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
