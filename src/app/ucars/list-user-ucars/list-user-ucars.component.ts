import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {Ucar} from "../../model/ucar.model";
import {ApiService} from "../../service/api.service";

@Component({
  selector: 'app-list-ucars',
  templateUrl: './list-user-ucars.component.html',
  styleUrls: ['./list-user-ucars.component.css']
})
export class ListUserUcarsComponent implements OnInit {

  ucars: Ucar[];

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    if(!window.localStorage.getItem('remember_token')) {
      this.router.navigate(['login']);
      return;
    }
    this.apiService.getUcars()
      .subscribe( data => {
        console.log(data.carlist);
        this.ucars = data.carlist;
      });
  }

  deleteUcars(ucars: Ucar): void {
    this.apiService.deleteUcars(ucars.id)
      .subscribe( data => {
        this.ucars = this.ucars.filter(u => u !== ucars);
        this.router.navigate(['list-user-ucars']);
      })
  };

  editUcars(ucars: Ucar): void {
    window.localStorage.removeItem("editUcarsId");
    window.localStorage.setItem("editUcarsId", ucars.id.toString());
    this.router.navigate(['edit-ucars']);
  };

  addUcars(): void {
    this.router.navigate(['add-ucars']);
  };
  //
  logout(): void {
    window.localStorage.setItem('remember_token', '');
    this.router.navigate(['list-ucars']);
  };
}
