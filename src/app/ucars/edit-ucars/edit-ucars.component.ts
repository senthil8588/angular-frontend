import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {Ucar} from "../../model/ucar.model";
import {ApiService} from "../../service/api.service";

@Component({
  selector: 'app-edit-ucars',
  templateUrl: './edit-ucars.component.html',
  styleUrls: ['./edit-ucars.component.css']
})
export class EditUcarsComponent implements OnInit {

  ucars: Ucar;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    let ucarsId = window.localStorage.getItem("editUcarsId");
    if(!ucarsId) {
      alert("Invalid action.")
      this.router.navigate(['list-user-ucars']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      carname: ['', Validators.required],
      carmodel: ['', Validators.required],
      company_name: ['', Validators.required],
      color: ['', Validators.required],
      mileage: ['', Validators.required],
      seating_capacity: ['', Validators.required],
      steering_type: ['', Validators.required],
      displacement_cc: ['', Validators.required],
      year: ['', Validators.required],
    });
    this.apiService.getUcarsById(+ucarsId)
      .subscribe( data => {
        this.editForm.setValue(data.carlist);
      });
  }

  onSubmit() {
    this.apiService.updateUcars(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data.status);

          if(data.status == 200) {
            alert('Car Details updated successfully.');
            this.router.navigate(['list-user-ucars']);
          }else {
            alert(data.message);
          }
        },
        error => {
          alert(error);
        });
  }

}
