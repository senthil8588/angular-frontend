import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUcarsComponent } from './edit-ucars.component';

describe('EditUcarsComponent', () => {
  let component: EditUcarsComponent;
  let fixture: ComponentFixture<EditUcarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUcarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUcarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
