import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {Ucar} from "../../model/ucar.model";
import {ApiService} from "../../service/api.service";

@Component({
  selector: 'app-list-ucars',
  templateUrl: './list-ucars.component.html',
  styleUrls: ['./list-ucars.component.css']
})
export class ListUcarsComponent implements OnInit {

  ucars: Ucar[];

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    // if(!window.localStorage.getItem('remember_token')) {
    //   this.router.navigate(['login']);
    //   return;
    // }
    this.apiService.getAllUcars()
      .subscribe( data => {
        console.log(data.carlist);
        this.ucars = data.carlist;
      });
  }



  addUcars(): void {
    if(!window.localStorage.getItem('remember_token')) {
      this.router.navigate(['login']);
      return;
    }
    this.router.navigate(['add-ucars']);
  };
  //

}
