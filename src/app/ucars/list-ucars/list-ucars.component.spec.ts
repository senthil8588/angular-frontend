import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUcarsComponent } from './list-ucars.component';

describe('ListUcarsComponent', () => {
  let component: ListUcarsComponent;
  let fixture: ComponentFixture<ListUcarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUcarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUcarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
